'use strict'; // Mode strict du JavaScript

const data = {
    name: "Action ou Vérité",
    active: true,
    verity: [{
            quote: "Quelle est la chose la plus embarrassante que vous ayez jamais faite ?",
            use: false
        },
        {
            quote: "Dis-moi un secret de ton enfance.",
            use: false
        },
        {
            quote: "Quel est ton plus grand vice (secret) ?",
            use: false
        },
        {
            quote: "Quelle est la chose la plus courageuse que vous ayez faite ?",
            use: false
        },
        {
            quote: "De quoi rêves-tu le plus ?",
            use: false
        },
        {
            quote: "Si vous pouviez désormais utiliser seulement 3 mots pour le reste de la soirée, lequel choisiriez-vous ?",
            use: false
        },
        {
            quote: "Quelle est la plus grosse erreur de ta vie ?",
            use: false
        },
        {
            quote: "Quel délit as-tu commis ?",
            use: false
        }
    ],
    action: [{
            quote: "Faites vous froncer les sourcils par un autre joueur.",
            use: false
        },
        {
            quote: "Laissez-vous coiffer par un coéquipier.",
            use: false
        },
        {
            quote: "Masser les pieds d’un joueur de votre choix.",
            use: false
        },
        {
            quote: "Echangez les vêtements avec l’autre joueur directement en face.",
            use: false
        },
        {
            quote: "Mange juste quelque chose sans utiliser tes mains.",
            use: false
        },
        {
            quote: "Pose 1 minute comme un bodybuilder.",
            use: false
        },
        {
            quote: "Buvez un grand verre de Coca-Cola",
            use: false
        },
        {
            quote: "Faites 5 pompes.",
            use: false
        }
    ]
}

let Players = [];

$(".value_player").click(function () {
    var nbr_player = $(this).val();
    setPlayer(nbr_player)
});

function setPlayer(nbrPlayer) {
    let i;
    for (i = 1; i <= nbrPlayer; i++) {
        Players.push("Joueur " + i);
    }
}

//--------- END
function endGame() {
    $('.game_start').css("display", "none");
    $('.game_player').css("display", "none");
    $('.game_choice').css("display", "none");
    $('.game_quote').css("display", "none");
    $('.game_end').css("display", "flex");
}

//--------- ACTION OU VERITÉ
let verity = data.verity;
let action = data.action;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function Choose(choice) {
    let number = getRandomInt(choice.length - 1)
    if ((choice[number].use == true)) {
        number = getRandomInt(choice.length - 1)
    }
    choice[number].use = true
    document.getElementById("quote").innerHTML = choice[number].quote;

}

//--------- FOWARD
$('#next_start').click(function () {
    $('.game_start').css("display", "none");
    $('.game_player').css("display", "flex");
});

$('.player_number').click(function () {
    $('.game_player').css("display", "none");
    $('.game_choice').css("display", "flex");
    let random_player = getRandomInt(Players.length + 1);
    $(".pyr_number").html(Players[random_player]);
});

$('.btn_choice').click(function () {
    $('.game_choice').css("display", "none");
    $('.game_quote').css("display", "flex");
});

$('.btn_quote').click(function () {
    $('.game_quote').css("display", "none");
    $('.game_choice').css("display", "flex");
    let random_player = getRandomInt(Players.length + 1);
    $(".pyr_number").html(Players[random_player]);
});

$('.end').click(function () {
    $('.game_start').css("display", "none");
    $('.game_player').css("display", "none");
    $('.game_choice').css("display", "none");
    $('.game_quote').css("display", "none");
    $('.game_end').css("display", "flex");
});